#include "pch.h"
#include "CollisionTrace.h"
#include "Globals.h"

void SetCollisionTraceParams(LPVOID fp, LPVOID rcx, UINT rdx, glm::vec4* r8, glm::vec3* r9, float s0, LPVOID s1, LPVOID s2, LPVOID s3)
{
    // maybe try to figure out the parameters' size and copy them instead?
    TraceParams.fp = (PCollisionTrace)fp;
    TraceParams.rcx = rcx; // maybe static, physics context?
    TraceParams.rdx = rdx;
    TraceParams.r8 = r8;
    TraceParams.r9 = r9;
    TraceParams.s0 = s0;
    TraceParams.s1 = s1; // float4* outHitPos
    TraceParams.s2 = s2; // 0
    TraceParams.s3 = s3;
}

// if there's another mod that hooks the trace function this need to be changed
bool UseCollisionTrace()
{
    if (hCameraOffset && !SetIgnoreCollisionHook)
    {
        return false;
    }
    if (!TraceParams.fp)
    {
        return false;
    }
    return Config.bUseCollision;
}

bool CallCollisionTrace(glm::vec4 origin, glm::vec3 offset, float radius, std::pair<bool, UINT> physParam)
{
    static BYTE zeroBuffer[0x100];

    assert(!hCameraOffset || (hCameraOffset && SetIgnoreCollisionHook));
    if (SetIgnoreCollisionHook)
    {
        (*SetIgnoreCollisionHook)(true);
    }
    std::memset(zeroBuffer, 0, sizeof(zeroBuffer));
    GTraceParams& param = TraceParams;
    bool hit = param.fp(param.rcx, physParam.first ? physParam.second : param.rdx, glm::value_ptr(origin), glm::value_ptr(offset), radius, zeroBuffer, 0, zeroBuffer + sizeof(glm::vec4));
    if (hit)
    {
        std::memcpy(&param.LastHitPos, zeroBuffer, sizeof(param.LastHitPos));
    }
    if (SetIgnoreCollisionHook)
    {
        (*SetIgnoreCollisionHook)(false);
    }
    return hit;
}