#include "pch.h"
#include "AutoTarget.h"
#include "Globals.h"
#include "CollisionTrace.h"

#include <glm/gtx/vector_angle.hpp>
#include <limits>
#include <cmath>

glm::vec4 CalcAutoTargetPos(glm::vec4 currentTarget, glm::vec4 playerPos, glm::vec3 cameraZ)
{
    //static bool bPrevIsAirborne = false;
    static bool bIsJumpAtk = false;
    static float AutoTargetInterp = 0.f;
    static float AutoTargetInterpY = 0.f;
    static FDelay AutoTargetDelay(0.25f);
    bool bUseAutoTarget = false;

    glm::mat4 matVP = glm::perspectiveLH(CameraFoV, 16.f / 9.f, std::numeric_limits<float>::min(), Config.AutoTargetMaxDistance) * glm::inverse(CameraTransform);
    glm::vec3 cameraZAligned = cameraZ * glm::vec3(1.f, 0.f, 1.f);
    // the camera shouldn't be able to point directly downwards but test it anyway
    cameraZAligned = glm::length(cameraZAligned) > 0.0001f ? glm::normalize(cameraZAligned) : glm::vec3(0.f, 0.f, 1.f);

#if 0
    // in mid-air this only detects the active frames of the swings, when the rotation-enablwed window already ended for many animations
    bUseAutoTarget = AnimationState.MovementFlags[64] || AnimationState.AnimationFlags[13] || AnimationState.AnimationFlags[9];
    bUseAutoTarget |= AnimationState.AnimationFlags[31] || AnimationState.AnimationFlags[32]; // weapon specific?
    bUseAutoTarget |= AnimationState.AnimationFlags[6]; // guarding

    //bool bIsHoldingBow = AnimationState.AnimationFlags[18];
    bool bIsAirborne = AnimationState.AnimationFlags[4];
    bool bIsLanding = AnimationState.AnimationFlags[15];

    bIsJumpAtk |= bUseAutoTarget && (bIsAirborne || bIsLanding);

    bIsJumpAtk &= bIsAirborne || bIsLanding /*|| AnimationState.AnimationFlags[23]*/;
    //bPrevIsAirborne = bIsAirborne;

    bool bIsJumpAtkStartup = (AnimationState.Bytes[0x90] == 0 && AnimationState.Bytes[0x91] == 0) || AnimationState.Bytes[0x93] == 0xff;

    bUseAutoTarget |= bIsJumpAtk || bIsJumpAtkStartup;
#endif
    bUseAutoTarget |= !AnimationState.MovementFlags[0]; // set to 0 for most actions (attacking, jumping, rolling, etc.), makes checking the other flags kinda meaningless.
    bUseAutoTarget |= AnimationState.AnimationFlags[6]; // guarding
    bUseAutoTarget &= !(AnimationState.MovementFlags[11] && !AnimationState.AnimationFlags[45]); // using items, except throwables
    //pAnimationState = nullptr;

    //if (!bIsIdle && bUseAutoTarget)
    //{
    //    AutoTargetDelay.Reset();
    //}
    //bUseAutoTarget = !AutoTargetDelay.Tick(DeltaTime_UpdateLockonTargets);
    AutoTargetInterp = InterpToF(AutoTargetInterp, float(bUseAutoTarget), bUseAutoTarget ? 0.f : 5.f, DeltaTime_UpdateLockonTargets);
    AutoTargetInterpY = AutoTargetInterp;// InterpToF(AutoTargetInterpY, float(bUseAutoTarget), bUseAutoTarget ? 0.f : 5.f, DeltaTime_UpdateLockonTargets);

    static FDynamicTargetBlend<glm::vec4> AutoTargetPosInterp(0.2f);
    static void* pPrevTarget = nullptr;
    std::pair<void*, glm::vec4> autoTarget = std::make_pair(nullptr, currentTarget);
    if (!LockonTargetPositions.empty())
    {
        float currentTargetWeights = -1.f;
        for (std::pair<void* const, glm::vec4>& lockonTarget : LockonTargetPositions)
        {
            const glm::vec4 targetPos = lockonTarget.second;
            glm::vec3 playerToTarget = targetPos.xyz - playerPos.xyz;
            glm::vec3 playerToTargetAligned = playerToTarget;
            playerToTargetAligned.y = 0;
            float distance = glm::length(playerToTarget);
            float angle = glm::length(playerToTarget) > 0.0001f ? glm::angle(cameraZ, glm::normalize(playerToTarget)) : 0.f;
            float angleAligned = glm::length(playerToTargetAligned) > 0.0001f ? glm::angle(cameraZAligned, glm::normalize(playerToTargetAligned)) : 0.f;
            if ((Config.AutoTargetMaxDistance <= 0.0001f || distance > Config.AutoTargetMaxDistance) || distance < 0.0001f
                || angleAligned >(Config.AutoTargetMaxAngle * 0.5f * DEG_TO_RAD))
            {
                continue;
            }

            glm::vec4 clipPos = matVP * (targetPos);
            clipPos /= clipPos.w;
            if (Config.bAutoTargetCheckFrustum)
            {

                if (glm::abs(clipPos.x / clipPos.w) > 1.f || glm::abs(clipPos.y / clipPos.w) > 1.f)
                {
                    continue;
                }
            }

            // some where the game itself already does a bunch of traces from the camera to enemies, which can be an option
            if (Config.bAutoTargetCheckCollision)
            {
                glm::vec4 traceOrigin = playerPos + glm::vec4(0.f, 1.25f, 0.f, 0.f);
                glm::vec3 traceOffset = targetPos - traceOrigin;
                bool hit = CallCollisionTrace(traceOrigin, traceOffset, Config.TraceRadius, std::pair(true, 0x5b));
                if (hit)
                {
                    continue;
                }
            }

            static constexpr const float clipPosScaleX = 1.f;
            static constexpr const float clipPosScaleY = 0.2f;
            static const float clipLengthScale = 1.f / std::sqrt(clipPosScaleX * clipPosScaleX + clipPosScaleY * clipPosScaleY);

            float distanceNormal = saturate(1.f - glm::inversesqrt(distance * distance + 1.0001f)); // saturate call can probably be removed
            float angleNormal = saturate(glm::length(clipPos.xy() * glm::vec2(clipPosScaleX, clipPosScaleY)) * clipLengthScale);
            float targetWeights = (1.f - distanceNormal) * max(0.f, Config.AutoTargetWeightDistance) + (1.f - angleNormal) * max(0.f, Config.AutoTargetWeightAngle);
            if (targetWeights > currentTargetWeights)
            {
                autoTarget = lockonTarget;
                currentTargetWeights = targetWeights;
            }
        }
    }
    if (pPrevTarget != autoTarget.first)
    {
        //LOG_DEBUG << autoTarget.first;
        AutoTargetPosInterp.Reset();
    }
    pPrevTarget = autoTarget.first;
    AutoTargetPosInterp.Update(autoTarget.second, DeltaTime_UpdateLockonTargets);
    currentTarget = lerp(currentTarget, AutoTargetPosInterp.GetValue(), glm::vec4(AutoTargetInterp, AutoTargetInterpY, AutoTargetInterp, 0.f));

    //LOG_DEBUG << glm::to_string(AutoTargetPosInterp);
#if USE_RESHADE
    {
        glm::vec4 clipPos = matVP * AutoTargetPosInterp.GetValue();
        HUDPos = glm::vec2(clipPos.xy() / clipPos.w);
    }
#endif

#ifdef _DEBUG
    //{
    //    auto log = LOG_PLAIN;
    //    log << "anim state: ";
    //    for (int i = 0; i < AnimationState.MovementFlags.size(); ++i)
    //    {
    //        log << AnimationState.MovementFlags[i];
    //    }
    //    log << " ";
    //    for (int i = 0; i < AnimationState.AnimationFlags.size(); ++i)
    //    {
    //        log << AnimationState.AnimationFlags[i];
    //    }
    //    for (int i = 0x90; i <= 0x93; ++i)
    //    {
    //        log << " " << std::hex << AnimationState.Bytes[i];
    //    }
    //    log << std::format(" {:.3}", AutoTargetInterp);
    //    log << '\n';
    //}
#endif

#ifdef _DEBUG
    //if (!LockonTargetPositions.empty())
    //{
    //    glm::vec3 nearestPos = LockonTargetPositions[0];
    //    for (auto& pos : std::vector(LockonTargetPositions.begin()+1, LockonTargetPositions.end()))
    //    {
    //        if (glm::distance(pAimData->pInput->PlayerPos.xyz(), pos.xyz()) < glm::distance(pAimData->pInput->PlayerPos.xyz(), nearestPos))
    //        {
    //            nearestPos = pos;
    //        }
    //    }

    //    glm::mat4 matVP = glm::perspectiveLH(CameraFoV, 16.f / 9.f, 0.1f, Config.AutoTargetMaxDistance) * glm::inverse(CameraTransform);
    //    glm::vec4 clipPos = matVP * glm::vec4(nearestPos, 1.f);
    //    LOG_DEBUG << glm::to_string(clipPos / clipPos.w);
    //}
#endif

    return currentTarget;
}
