

extern hk_Collision : proto
extern hkCaller_Collision : qword

.code
	prehk_Collision proc
		mov rax, [rsp]
		sub rax, 5
		mov [hkCaller_Collision], rax
		jmp [hk_Collision]
	prehk_Collision endp
end
