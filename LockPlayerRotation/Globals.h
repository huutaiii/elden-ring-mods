#pragma once

#include <vector>
#include <glm-dx.h>

#include <bitset>
#include <unordered_map>

#ifndef DEG_TO_RAD
#define DEG_TO_RAD ((float)(0.017453292519943295f))
#endif
#ifndef RAD_TO_DEG
#define RAD_TO_DEG ((float)(57.29577951308232f))
#endif

#ifdef _DEBUG
#ifndef USE_RESHADE
#define USE_RESHADE 1
#endif
#endif

#ifndef USE_RESHADE
#define USE_RESHADE 0
#endif

class FDelay
{
protected:
    float ElapsedTime = 0.f;
    float Duration = 1.f;
public:
    FDelay(float duration = 1.f) : Duration(duration), ElapsedTime(duration) {}
    void Reset() { ElapsedTime = 0.f; }
    bool Tick(float deltaTime)
    {
        if (ElapsedTime < Duration)
        {
            ElapsedTime += deltaTime;
        }
        return ElapsedTime >= Duration;
    }
};

static_assert(sizeof(void*) == 8);
union GAnimationState
{
    struct
    {
	    void* vt;
	    PAD(sizeof(void*));

        std::bitset<128> MovementFlags; // +10
        PAD(0x20);
        std::bitset<64> AnimationFlags; // +40
        PAD(0x80 - 0x48);
        glm::vec4 RotationRate; // in degrees
    };
    unsigned char Bytes[0x100];
};

typedef bool (*PCollisionTrace)(LPVOID, UINT, LPVOID, LPVOID, float, LPVOID, LPVOID, LPVOID);
struct GTraceParams
{
    PCollisionTrace fp;
    LPVOID rcx;
    UINT rdx;
    glm::vec4* r8; // trace start world pos
    glm::vec3* r9; // trace offset
    float s0; // sphere radius
    LPVOID s1;
    LPVOID s2;
    LPVOID s3;

    glm::vec4 LastHitPos;
};
extern GTraceParams TraceParams;

struct UConfig
{
    bool bUseLookAt = true;
    bool bUseInputOverride = false;

    bool bAlwaysTurn = false;
    bool bStartEnabled = true;

    bool bRemoveSpellAimDeadzone = false;

    bool bUseCollision = false;
    bool bTraceOffsetMaxDistance = false;
    float TraceStartOffset = 0.f;
    float TraceDistance = 100.f;
    float TraceRadius = 0.01f;

    bool bUseAutoTarget = false;
    float AutoTargetMaxDistance = 0.f;
    float AutoTargetMaxAngle = 90.f;
    float AutoTargetWeightDistance = 1.f;
    float AutoTargetWeightAngle = 1.f;
    bool bAutoTargetCheckCollision = false;
    bool bAutoTargetCheckFrustum = false;

    UHotKey KeyToggle;

    void Read(std::string path);
};
extern UConfig Config;

extern HMODULE hCameraOffset;
extern void (*SetIgnoreCollisionHook)(bool);

extern std::unordered_map<void*, glm::vec4> LockonTargetPositions;
extern float DeltaTime_UpdateLockonTargets;
extern GAnimationState AnimationState;


extern glm::mat4 CameraTransform;
extern float CameraFoV; // vertical fov in radians
extern glm::vec4 PivotPosUninterp;
extern float CameraMaxDistance;

extern bool bIsSprinting;
extern bool bIsOnTorrent;
extern bool bHeadTracking;

extern bool bCollisionHit;
extern float CollisionHitDistance;
extern glm::vec4 CollisionHitPos;
extern glm::vec4 CollisionTraceEnd;
extern glm::vec4 CollisionPosInterp;

extern bool bEnabled;

extern bool bHasLockon;
extern bool bIsIdle;
extern bool bAnimDirectional;

#if USE_RESHADE
extern glm::vec2 HUDPos;
#endif
