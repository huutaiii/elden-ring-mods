

// 1.10
eldenring.exe+3D75EE - 0F10 90 D0000000      - movups xmm2,[rax+000000D0]	; reads lockon target coords (intermediate)
eldenring.exe+3D75F5 - 48 8D 55 07           - lea rdx,[rbp+07]
eldenring.exe+3D75F9 - 48 8D 4D F7           - lea rcx,[rbp-09]
eldenring.exe+3D75FD - 0F28 C2               - movaps xmm0,xmm2
eldenring.exe+3D7600 - 0F28 CA               - movaps xmm1,xmm2
eldenring.exe+3D7603 - F3 0F5C 03            - subss xmm0,[rbx]
eldenring.exe+3D7607 - 0FC6 CA 55            - shufps xmm1,xmm2,55
eldenring.exe+3D760B - F3 0F5C 4B 04         - subss xmm1,[rbx+04]
eldenring.exe+3D7610 - F3 0F11 45 07         - movss [rbp+07],xmm0
eldenring.exe+3D7615 - 0F28 C2               - movaps xmm0,xmm2
eldenring.exe+3D7618 - 0FC6 C2 AA            - shufps xmm0,xmm2,-56
eldenring.exe+3D761C - F3 0F5C 43 08         - subss xmm0,[rbx+08]
eldenring.exe+3D7621 - 0FC6 D2 FF            - shufps xmm2,xmm2,-01
eldenring.exe+3D7625 - F3 0F5C 53 0C         - subss xmm2,[rbx+0C]
eldenring.exe+3D762A - F3 0F11 4D 0B         - movss [rbp+0B],xmm1
eldenring.exe+3D762F - F3 0F11 45 0F         - movss [rbp+0F],xmm0
eldenring.exe+3D7634 - F3 0F11 55 13         - movss [rbp+13],xmm2
eldenring.exe+3D7639 - 0F57 D2               - xorps xmm2,xmm2
eldenring.exe+3D763C - E8 4F49DBFF           - call eldenring.exe+18BF90	; turns coords into normal
eldenring.exe+3D7641 - 0F28 00               - movaps xmm0,[rax]
eldenring.exe+3D7644 - 66 0F7F 07            - movdqa [rdi],xmm0			; is clamped by caller


eldenring.exe+3F8E4A - F3 0F11 44 24 3C      - movss [rsp+3C],xmm0
eldenring.exe+3F8E50 - 0F28 44 24 30         - movaps xmm0,[rsp+30]
eldenring.exe+3F8E55 - 66 0F7F 45 90         - movdqa [rbp-70],xmm0
--------------------------------------------------------------
eldenring.exe+3F8E5A - 76 11                 - jna eldenring.exe+3F8E6D		; jumps if pitch > 0 (?)
eldenring.exe+3F8E5C - F3 0F10 45 A0         - movss xmm0,[rbp-60]
eldenring.exe+3F8E61 - 0F2F C6               - comiss xmm0,xmm6				; xmm0 == deadzone
eldenring.exe+3F8E64 - 76 07                 - jna eldenring.exe+3F8E6D		; jumps if pitch > deadzone
eldenring.exe+3F8E66 - C7 45 90 00000000     - mov [rbp-70],00000000		; clamps aim angle
--------------------------------------------------------------
eldenring.exe+3F8E6D - F3 0F10 4D 94         - movss xmm1,[rbp-6C]
eldenring.exe+3F8E72 - 0F2F 4D C4            - comiss xmm1,[rbp-3C]
eldenring.exe+3F8E76 - 76 11                 - jna eldenring.exe+3F8E89
eldenring.exe+3F8E78 - F3 0F10 45 A4         - movss xmm0,[rbp-5C]


1. it seems that the direction for arrows & bolts are only determined by the skeleton
and the snapping/deadzone is applied to the rigs for the entire animations instead of just on the projectiles when they spawn
it's still applied when locked-on
1.0.1 there is in fact some correction for lockon target position... but the animations still have deadzones...
1.1. bow spells are controlled like bow&arrows and probably have bigger deadzones

2. there is probably some kind of offset correction for aiming with the crosshair. maybe I can reuse that for 3rd person aim with camera offset?


1.10
called for any projectile (pc, npc)
eldenring.exe+391590 - EB 30                 - jmp eldenring.exe+3915C2
eldenring.exe+391592 - 45 8B C6              - mov r8d,r14d
eldenring.exe+391595 - 48 8D 55 D0           - lea rdx,[rbp-30]
eldenring.exe+391599 - 48 8B CB              - mov rcx,rbx
eldenring.exe+39159C - E8 0FDCFFFF           - call eldenring.exe+38F1B0
eldenring.exe+3915A1 - 0F28 20               - movaps xmm4,[rax]
eldenring.exe+3915A4 - 0F28 DC               - movaps xmm3,xmm4
eldenring.exe+3915A7 - 0FC6 58 10 44         - shufps xmm3,[rax+10],44
eldenring.exe+3915AC - 0FC6 60 10 EE         - shufps xmm4,[rax+10],-12
eldenring.exe+3915B1 - 0F28 50 20            - movaps xmm2,[rax+20]
eldenring.exe+3915B5 - 0F28 CA               - movaps xmm1,xmm2
eldenring.exe+3915B8 - 0FC6 48 30 44         - shufps xmm1,[rax+30],44
eldenring.exe+3915BD - 0FC6 50 30 EE         - shufps xmm2,[rax+30],-12
eldenring.exe+3915C2 - 0F28 C3               - movaps xmm0,xmm3

1.10
locked on
eldenring.exe+392FBE - 66 0F7F 07            - movdqa [rdi],xmm0
eldenring.exe+392FC2 - E9 DC000000           - jmp eldenring.exe+3930A3
eldenring.exe+392FC7 - 48 8B 41 10           - mov rax,[rcx+10]				; rax = probably target address
eldenring.exe+392FCB - 48 85 C0              - test rax,rax
eldenring.exe+392FCE - 74 15                 - je eldenring.exe+392FE5
eldenring.exe+392FD0 - 83 78 78 FF           - cmp dword ptr [rax+78],-01
eldenring.exe+392FD4 - 0F84 C9000000         - je eldenring.exe+3930A3
eldenring.exe+392FDA - 0F10 00               - movups xmm0,[rax]			; target coords
eldenring.exe+392FDD - 0F29 02               - movaps [rdx],xmm0
eldenring.exe+392FE0 - E9 BE000000           - jmp eldenring.exe+3930A3
eldenring.exe+392FE5 - E8 060D0000           - call eldenring.exe+393CF0
eldenring.exe+392FEA - 48 85 C0              - test rax,rax
eldenring.exe+392FED - 0F84 B0000000         - je eldenring.exe+3930A3
eldenring.exe+392FF3 - 8B 4B 30              - mov ecx,[rbx+30]
