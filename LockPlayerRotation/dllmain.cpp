// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"

#include "MinHook.h"
#pragma comment(lib, "../bin/MinHook.x64.lib")
#pragma comment(lib, "xinput.lib")

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <filesystem>
#include <vector>
#include <memory>
#include <bitset>

#include <reshade/reshade.hpp>
#include <reshade/reshade_api.hpp>
namespace rapi = reshade::api;

#include "Hooks.h"
#include "Globals.h"

void UConfig::Read(std::string path)
{
    INIReader ini(path);
    KeyToggle = UHotKey(ini.Get("main", "toggle-key", ""));
    bAlwaysTurn = ini.GetBoolean("main", "always-turning", bAlwaysTurn);
    bStartEnabled = ini.GetBoolean("main", "start-locked", bStartEnabled);

    bUseCollision = ini.GetBoolean("collision-trace", "use-collision-trace", bUseCollision);
    TraceDistance = ini.GetFloat("collision-trace", "trace-distance", TraceDistance);
    TraceStartOffset = ini.GetFloat("collision-trace", "trace-start-offset", TraceStartOffset);
    bTraceOffsetMaxDistance = ini.GetBoolean("collision-trace", "trace-offset-max-distance", bTraceOffsetMaxDistance);

    bUseAutoTarget = ini.GetBoolean("auto-targeting", "use-auto-targeting", bUseAutoTarget);
    AutoTargetMaxDistance = ini.GetFloat("auto-targeting", "search-distance", AutoTargetMaxDistance);
    AutoTargetMaxAngle = ini.GetFloat("auto-targeting", "search-angle", AutoTargetMaxAngle);
    AutoTargetWeightDistance = ini.GetFloat("auto-targeting", "weight-distance", AutoTargetWeightDistance);
    AutoTargetWeightAngle = ini.GetFloat("auto-targeting", "weight-angle", AutoTargetWeightAngle);
    bAutoTargetCheckCollision = ini.GetBoolean("auto-targeting", "check-collision", bAutoTargetCheckCollision);
    bAutoTargetCheckFrustum = ini.GetBoolean("auto-targeting", "exclude-offscreen", bAutoTargetCheckFrustum);

    bRemoveSpellAimDeadzone = ini.GetBoolean("experimental", "remove-spell-aim-deadzone", bRemoveSpellAimDeadzone);
}


WNDPROC PrevWndProc = NULL;
LRESULT WINAPI HookWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    bool bForwardMessage = true;
    if (Config.KeyToggle.IsPressed(msg, wParam, lParam))
    {
        bForwardMessage = false;
        bEnabled = !bEnabled;
    }
    if (/*bForwardMessage &&*/ PrevWndProc)
    {
        return CallWindowProcW(PrevWndProc, hwnd, msg, wParam, lParam);
    }
    return 0;
}

std::vector<std::unique_ptr<UMinHook>> Hooks;

DWORD WINAPI MainThread(LPVOID lpParams)
{
    MH_STATUS mh;

    mh = MH_Initialize();
    //ModUtils::Log("%s", MH_StatusToString(mh));

    if (!InitHooks())
    {
        LOG_WARNING << "Failed to create one or more function hook(s)";
    }

    bEnabled = Config.bStartEnabled;

    return 0;
}

DWORD WINAPI ThreadHookWndProc(LPVOID)
{
    //Sleep(5000);

    LOG_INFO << "Attempting to hook WNDPROC";
    HWND hwnd = FindWindowHandle(L"ELDEN RING", true);
    if (!hwnd)
    {
        LOG_INFO << "Can't find window handle";
    }
    else
    {
        LOG_DEBUG << "found hwnd " << (void*)(hwnd);
        SetLastError(0);
        PrevWndProc = (WNDPROC)SetWindowLongPtrW(hwnd, GWLP_WNDPROC, LONG_PTR(&HookWndProc));
        LOG.println("WNDPROC hook: %d %p %p", GetLastError(), PrevWndProc, &HookWndProc);
    }
    return 0;
}

DWORD WINAPI ThreadLoadDependencies(LPVOID lpParams)
{
    for (unsigned int count = 0; count < 10 && !SetIgnoreCollisionHook; ++count)
    {
        Sleep(2000);
        LOG_DEBUG << "looking for CameraOffset";
        hCameraOffset = GetModuleHandleA("CameraOffset.dll");
        if (hCameraOffset)
        {
            LOG_INFO << "Found CameraOffset: " << hCameraOffset;
            SetLastError(0);
            SetIgnoreCollisionHook = (decltype(SetIgnoreCollisionHook))GetProcAddress(hCameraOffset, "SetIgnoreCollisionHook");
            if (!SetIgnoreCollisionHook)
            {
                LOG_WARNING << "Unsupported CameraOffset version, collision-trace-based features won't work properly";
            }
            break;
        }
    }
    return 0;
}

DWORD WINAPI ConfigWatchThread(LPVOID lpParam)
{
    HMODULE hModule = (HMODULE)lpParam;
    static constexpr size_t FILE_NOTIFY_BUFFER_SIZE = 1024;
    ULog::Get().dprintln("Initializing Config file watch thread");
    std::filesystem::path modDir = std::filesystem::path(GetWinAPIString(GetModuleFileNameW, GetCurrentModule())).parent_path() / TARGET_NAME;
    ULog::Get().dprintln("watching directory: %s", modDir.c_str());
    while (1)
    {
        char notifybuf[FILE_NOTIFY_BUFFER_SIZE];
        DWORD numBytes;
        HANDLE hDir = CreateFileW(modDir.wstring().c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, 0);
        if (ReadDirectoryChangesW(hDir, &notifybuf, FILE_NOTIFY_BUFFER_SIZE, FALSE, FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_FILE_NAME, &numBytes, 0, 0))
        {
            FILE_NOTIFY_INFORMATION* pNotify = reinterpret_cast<FILE_NOTIFY_INFORMATION*>(&notifybuf);
            if (numBytes)
            {
                while (1)
                {
                    if (pNotify->Action == FILE_ACTION_MODIFIED || pNotify->Action == FILE_ACTION_RENAMED_NEW_NAME || pNotify->Action == FILE_ACTION_ADDED)
                    {
                        std::wstring wsName(pNotify->FileName, pNotify->FileNameLength / sizeof(WCHAR));
                        if (wsName == L"config.ini")
                        {
                            ULog::Get().println("Config file changed");
                            Sleep(300); // workaround for some editors like vscode
                            Config = UConfig();
                            Config.Read((modDir / "config.ini").string());
                        }
                    }
                    if (pNotify->NextEntryOffset == 0) break;
                    pNotify += pNotify->NextEntryOffset;
                }
            }
            else
            {
                ULog::Get().eprintln("Couldn't setup config file watch");
                break;
            }
        }
    }
    return 0;
}

#if USE_RESHADE
void SetHUDPosition(glm::vec2 pos)
{
    HUDPos = pos;
}

void AddonBeginEffects(rapi::effect_runtime* runtime, rapi::command_list* cmd_list, rapi::resource_view rtv, rapi::resource_view rtv_srgb)
{
    auto uniformX = runtime->find_uniform_variable("xhair.fx", "OffsetX");
    auto uniformY = runtime->find_uniform_variable("xhair.fx", "OffsetY");
    if (uniformX.handle && uniformY.handle)
    {
        runtime->set_uniform_value_float(uniformX, HUDPos.x);
        runtime->set_uniform_value_float(uniformY, HUDPos.y);
    }
}

void RegisterAddon(HMODULE hModule)
{
    if (!reshade::register_addon(hModule))
    {
        assert(0);
    }
    reshade::register_event<reshade::addon_event::reshade_begin_effects>(AddonBeginEffects);
}
#endif

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {
        //DisableThreadLibraryCalls(hModule);
        std::filesystem::path modDir = std::filesystem::path(GetWinAPIString(GetModuleFileNameW, GetCurrentModule())).parent_path() / TARGET_NAME;
        ULog::FileName = (modDir / std::string(TARGET_NAME ".log")).string();
        ULog::ModuleName = TARGET_NAME;
        if (TryLoadLibrary((modDir.parent_path() / "bin" / "MinHook.x64.dll").wstring()) == NULL)
        {
            return FALSE;
        }
        Config.Read((modDir / "config.ini").string());
        LOG_DEBUG << &Config;
        CreateThread(0, 0, &MainThread, 0, 0, 0);
        CreateThread(0, 0, &ThreadHookWndProc, 0, 0, 0);
        CreateThread(0, 0, &ThreadLoadDependencies, 0, 0, 0);
        CreateThread(0, 0, &ConfigWatchThread, hModule, 0, 0);
#if USE_RESHADE
        RegisterAddon(hModule);
#endif
        break;
    }
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

