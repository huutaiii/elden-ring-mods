#include "pch.h"
#include "Hooks.h"
#include "Globals.h"
#include "CollisionTrace.h"
#include "AutoTarget.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/vector_angle.hpp>

constexpr const uint16_t MASK = 0xffff;

/*
eldenring.exe+3AFDD8 - 83 F8 07              - cmp eax,07
eldenring.exe+3AFDDB - 0F87 92040000         - ja eldenring.exe+3B0273
eldenring.exe+3AFDE1 - 41 8B 8C 85 CC023B00  - mov ecx,[r13+rax*4+003B02CC]
eldenring.exe+3AFDE9 - 49 03 CD              - add rcx,r13
eldenring.exe+3AFDEC - FF E1                 - jmp rcx
eldenring.exe+3AFDEE - 48 8B 4F 60           - mov rcx,[rdi+60]
eldenring.exe+3AFDF2 - 4D 8B CE              - mov r9,r14
eldenring.exe+3AFDF5 - 4C 8B C6              - mov r8,rsi
eldenring.exe+3AFDF8 - 41 0F28 C9            - movaps xmm1,xmm9
eldenring.exe+3AFDFC - E8 DF410000           - call eldenring.exe+3B3FE0

83 F8 07 0F87 ??????00 41 8B 8C 85 ???????? 49 03 CD FF E1 48 8B 4F ?? 4D 8B CE 4C 8B C6 41 0F28 C9 E8 ??????00
*/
std::vector<UINT16> PATTERN_CAMERA_TICK = { 0x83, 0xF8, 0x07, 0x0F, 0x87, MASK, MASK, MASK, 0x00, 0x41, 0x8B, 0x8C, 0x85, MASK, MASK, MASK, MASK, 0x49, 0x03, 0xCD, 0xFF, 0xE1, 0x48, 0x8B, 0x4F, MASK, 0x4D, 0x8B, 0xCE, 0x4C, 0x8B, 0xC6, 0x41, 0x0F, 0x28, 0xC9, 0xE8, MASK, MASK, MASK, 0x00 };

/*
eldenring.exe+708AD8 - E8 A3C0CEFF           - call eldenring.exe+3F4B80
eldenring.exe+708ADD - 80 BE 8E290000 00     - cmp byte ptr [rsi+0000298E],00
eldenring.exe+708AE4 - 74 31                 - je eldenring.exe+708B17
eldenring.exe+708AE6 - 41 0F28 CD            - movaps xmm1,xmm13
eldenring.exe+708AEA - 48 8B CE              - mov rcx,rsi
eldenring.exe+708AED - 80 BE 8F290000 00     - cmp byte ptr [rsi+0000298F],00
eldenring.exe+708AF4 - 74 07                 - je eldenring.exe+708AFD
eldenring.exe+708AF6 - E8 B50D0000           - call eldenring.exe+7098B0
eldenring.exe+708AFB - EB 05                 - jmp eldenring.exe+708B02
eldenring.exe+708AFD - E8 1E120000           - call eldenring.exe+709D20
eldenring.exe+708B02 - E8 D9CB6F00           - call eldenring.exe+E056E0

E8 ??????FF 80 BE ????0000 00 74 ?? 41 0F28 CD 48 8B CE 80 BE ????0000 00 74 07 E8 ??????00 EB 05 E8 ??????00 E8 ??????00
*/
std::vector<UINT16> PATTERN_PLAYER_AIM = { 0xE8, MASK, MASK, MASK, 0xFF, 0x80, 0xBE, MASK, MASK, 0x00, 0x00, 0x00, 0x74, MASK, 0x41, 0x0F, 0x28, 0xCD, 0x48, 0x8B, 0xCE, 0x80, 0xBE, MASK, MASK, 0x00, 0x00, 0x00, 0x74, 0x07, 0xE8, MASK, MASK, MASK, 0x00, 0xEB, 0x05, 0xE8, MASK, MASK, MASK, 0x00, 0xE8, MASK, MASK, MASK, 0x00 };

/*
eldenring.exe+40863A - E8 B193FEFF           - call eldenring.exe+3F19F0
eldenring.exe+40863F - 84 C0                 - test al,al
eldenring.exe+408641 - 75 B8                 - jne eldenring.exe+4085FB
eldenring.exe+408643 - 49 8B CE              - mov rcx,r14
eldenring.exe+408646 - E8 85190300           - call eldenring.exe+439FD0
eldenring.exe+40864B - 48 8B C8              - mov rcx,rax
eldenring.exe+40864E - E8 7D9BFEFF           - call eldenring.exe+3F21D0
eldenring.exe+408653 - 84 C0                 - test al,al
eldenring.exe+408655 - 75 A4                 - jne eldenring.exe+4085FB

E8 ??????FF 84 C0 75 ?? 49 8B CE E8 ??????00 48 8B C8 E8 ??????FF 84 C0 75 ??
*/
std::vector<UINT16> PATTERN_LOCKON_STATE = { 0xE8, MASK, MASK, MASK, 0xFF, 0x84, 0xC0, 0x75, MASK, 0x49, 0x8B, 0xCE, 0xE8, MASK, MASK, MASK, 0x00, 0x48, 0x8B, 0xC8, 0xE8, MASK, MASK, MASK, 0xFF, 0x84, 0xC0, 0x75, MASK };

/*
eldenring.exe+408360 - 40 53                 - push rbx
eldenring.exe+408362 - 48 83 EC 20           - sub rsp,20
eldenring.exe+408366 - 48 8B D9              - mov rbx,rcx
eldenring.exe+408369 - E8 52020000           - call eldenring.exe+4085C0
eldenring.exe+40836E - 48 89 43 70           - mov [rbx+70],rax
eldenring.exe+408372 - 48 83 C4 20           - add rsp,20
eldenring.exe+408376 - 5B                    - pop rbx
eldenring.exe+408377 - C3                    - ret

40 53 48 83 EC 20 48 8B D9 E8 ??????00 48 89 43 ?? 48 83 C4 20 5B C3
*/
std::vector<UINT16> PATTERN_SPELL_AIM = { 0x40, 0x53, 0x48, 0x83, 0xEC, 0x20, 0x48, 0x8B, 0xD9, 0xE8, MASK, MASK, MASK, 0x00, 0x48, 0x89, 0x43, MASK, 0x48, 0x83, 0xC4, 0x20, 0x5B, 0xC3 };

/*
eldenring.exe+3D9C69 - 74 0F                 - je eldenring.exe+3D9C7A
eldenring.exe+3D9C6B - F3 0F10 4E 08         - movss xmm1,[rsi+08]
eldenring.exe+3D9C70 - 48 8B CF              - mov rcx,rdi
eldenring.exe+3D9C73 - E8 D8040000           - call eldenring.exe+3DA150
eldenring.exe+3D9C78 - EB 2A                 - jmp eldenring.exe+3D9CA4

74 0F F3 0F10 4E 08 48 8B CF E8 ??????00 EB 2A
*/
std::vector<UINT16> PATTERN_FREE_MOVEMENT = { 0x74, 0x0F, 0xF3, 0x0F, 0x10, 0x4E, 0x08, 0x48, 0x8B, 0xCF, 0xE8, MASK, MASK, MASK, 0x00, 0xEB, 0x2A };

/*
eldenring.exe+3DA694 - E8 A717FFFF           - call eldenring.exe+3CBE40
eldenring.exe+3DA699 - 45 0F57 CB            - xorps xmm9,xmm11
eldenring.exe+3DA69D - 41 0F28 C1            - movaps xmm0,xmm9
eldenring.exe+3DA6A1 - F3 0F59 05 97EF1704   - mulss xmm0,[eldenring.exe+4559640]
eldenring.exe+3DA6A9 - F3 0F11 45 A0         - movss [rbp-60],xmm0

E8 ??????FF 45 0F57 CB 41 0F28 C1 F3 0F59 05 ???????? F3 0F11 45 A0
*/
std::vector<UINT16> PATTERN_SET_ROTATION = { 0xE8, MASK, MASK, MASK, 0xFF, 0x45, 0x0F, 0x57, 0xCB, 0x41, 0x0F, 0x28, 0xC1, 0xF3, 0x0F, 0x59, 0x05, MASK, MASK, MASK, MASK, 0xF3, 0x0F, 0x11, 0x45, 0xA0 };

/*
eldenring.exe+40AE28 - E8 C36BFEFF           - call eldenring.exe+3F19F0
eldenring.exe+40AE2D - 84 C0                 - test al,al
eldenring.exe+40AE2F - 0F84 54090000         - je eldenring.exe+40B789
eldenring.exe+40AE35 - 0F57 D2               - xorps xmm2,xmm2
eldenring.exe+40AE38 - BA 02000000           - mov edx,00000002
eldenring.exe+40AE3D - 49 8B CE              - mov rcx,r14
eldenring.exe+40AE40 - E8 FB377C00           - call eldenring.exe+BCE640

E8 ???????? 84 C0 0F84 ????0000 0F57 D2 BA 02000000 49 8B CE E8 ????????
*/
std::vector<UINT16> PATTERN_DIRECTIONAL_ANIMATION = { 0xE8, MASK, MASK, MASK, MASK, 0x84, 0xC0, 0x0F, 0x84, MASK, MASK, 0x00, 0x00, 0x0F, 0x57, 0xD2, 0xBA, 0x02, 0x00, 0x00, 0x00, 0x49, 0x8B, 0xCE, 0xE8, MASK, MASK, MASK, MASK };


static_assert(sizeof(glm::vec4) == 0x10);
struct TPlayerInput
{
    PAD(0x198);
    bool bIsIdle;
    bool bCanRotate; //?
    PAD(0xC6);
    // +270
    glm::vec4 PlayerPos; // points to the feet
    PAD(0xE0);
    // +350
    glm::vec4 InputDir; // relative to player char
    PAD(0x40);
    // +3A0
    float InputScale; // == 2.0 when sprinting
    float InputAngleDeg;
};


struct TPlayerAim
{
    PAD(0x58);
    TPlayerInput* pInput; // +58 (8B)
    PAD(0x69); // nice
    // affects head/torso tracking, directional animations (roll, jump, etc.) and maybe more
    bool bLookAt; // +C9 (1B)
    PAD(0x6);
    glm::vec4 Target; // +D0 (16B)
    PAD(0x454);
    // stamina recovery rate? why is it stored here?
    uint32_t StaminaRecovery; // +534
};

void (*tram_PlayerAim)(PVOID, bool);
void hk_PLayerAim(TPlayerAim* pAimData, bool bHasLockon)
{
    //bIsSprinting = pAimData->pInput->InputScale > 1.f;
    ::bHasLockon = bHasLockon;
    if (!bEnabled || bHasLockon)
    {
        tram_PlayerAim(pAimData, bHasLockon);
        return;
    }

    //LOG_DEBUG << pAimData->MovementFlags << " " << pAimData->AnimationFlags;

    bool bInputOverride = false;

    if (!Config.bAlwaysTurn)
    {
        bIsIdle = pAimData->StaminaRecovery > 0 && pAimData->pInput->InputScale == 0.f;
        //bIsIdle &= AnimationState.MovementFlags[0] && AnimationState.AnimationFlags.none();
        bIsIdle &= !AnimationState.AnimationFlags[6];
        //bIsIdle |= AnimationState.MovementFlags[11] && AnimationState.AnimationFlags.none();
    }
    //if (Config.bUseInputOverride)
    //{
    //    bInputOverride = (pAimData->StaminaRecovery == 0 && pAimData->pInput->InputScale > 0.1f);
    //}

    bool bChangeRotation = !bIsIdle && !bInputOverride;

    glm::vec4 cameraPos = CameraTransform[3];
    glm::vec3 cameraZ = CameraTransform * glm::vec4(0, 0, 1, 0);

    if (UseCollisionTrace())
    {
        pAimData->Target = CollisionHitPos;
    }
    else
    {
        pAimData->Target = glm::vec4(PivotPosUninterp.xyz + cameraZ * 100.f, 1.f);
    }

    if (Config.bUseAutoTarget)
    {
        pAimData->Target = CalcAutoTargetPos(pAimData->Target, pAimData->pInput->PlayerPos, cameraZ);
    }

#if USE_RESHADE
    //{
    //    glm::mat4 matVP = glm::perspectiveLH(CameraFoV, 16.f / 9.f, FLT_MIN, FLT_MAX) * glm::inverse(CameraTransform);
    //    glm::vec4 clipPos = matVP * pAimData->Target;
    //    HUDPos = glm::vec2(clipPos.xy() / clipPos.w);
    //}
#endif

    tram_PlayerAim(pAimData, bChangeRotation);

    if (Config.bUseLookAt)
    {
        pAimData->bLookAt = true;
    }

    if (bHeadTracking)
    {
        pAimData->bLookAt = false;
    }

    //bAnimDirectional = false;
}

void (*tram_CameraTick)(LPVOID, float, LPVOID, LPVOID);
void hk_CameraTick(LPVOID rcx, float xmm1, LPVOID r8, LPVOID r9)
{
    tram_CameraTick(rcx, xmm1, r8, r9);

    //PrevPivotPos = PivotPosUninterp;
    memcpy(glm::value_ptr(CameraTransform), LPVOID(UINT_PTR(rcx) + 0x10), sizeof(float) * 16);
    //CameraTransform[3] = glm::vec4(0, 0, 0, 1);
    CameraFoV = *reinterpret_cast<float*>(reinterpret_cast<uintptr_t>(rcx) + 0x50);

    memcpy(glm::value_ptr(PivotPosUninterp), LPVOID(UINT_PTR(rcx) + 0xB0), sizeof(float) * 4);
    CameraMaxDistance = *((float*)(LPBYTE(rcx) + 0x1B4));

    // the camera ticks after hk_PlayerAim is called so the collision is off by one frame but it's safer i guess.
    if (UseCollisionTrace())
    {
        float blendw = 0.25f;
        glm::vec4 cameraPos = CameraTransform[3];
        glm::vec3 cameraZ = CameraTransform * glm::vec4(0, 0, 1, 0);
        float startOffset = (Config.bTraceOffsetMaxDistance ? CameraMaxDistance : 0.f) + Config.TraceStartOffset;

        glm::vec4 traceOrigin = glm::vec4(cameraPos.xyz + cameraZ * (startOffset - blendw), 1.f);
        glm::vec3 traceOffset = cameraZ * Config.TraceDistance;
        CollisionTraceEnd = glm::vec4(traceOrigin.xyz + traceOffset, 1.f);

        bool hitD = CallCollisionTrace(traceOrigin, traceOffset, Config.TraceRadius, std::pair(true, 0x31));
        glm::vec4 posD = hitD ? (TraceParams.LastHitPos) : CollisionTraceEnd;
        float distD = glm::distance(traceOrigin, posD);

        bool hitS = CallCollisionTrace(traceOrigin, traceOffset, Config.TraceRadius, std::pair(true, 5));
        glm::vec4 posS = hitS ? (TraceParams.LastHitPos) : CollisionTraceEnd;
        float distS = glm::distance(traceOrigin, posS);

        bCollisionHit = hitS || hitD;
        CollisionHitPos = bCollisionHit ? (distS < distD ? posS : posD) : CollisionTraceEnd;
        CollisionHitDistance = glm::distance(traceOrigin.xyz(), CollisionHitPos.xyz());

        float a = smoothstep(startOffset - blendw, startOffset, CollisionHitDistance);
        CollisionHitPos = lerp(CollisionTraceEnd, CollisionHitPos, a);
        CollisionPosInterp = lerp(CollisionPosInterp, CollisionHitPos, saturate(xmm1 * 10.f));

        //LOG_DEBUG << LOG_CONTEXT << bCollisionHit << " " << (bCollisionHit ? std::format("{} {}", glm::to_string(CollisionHitPos), CollisionHitDistance) : "");
    }

    //PivotVelocity = (PivotPosUninterp.xyz - PrevPivotPos.xyz) * (1.f / xmm1);
}

bool bCallingSpellAim;
bool bCallingDirectionalAnim;

// affects directional rolls/jumps, look-at animations
bool (*tram_GetLockonState)(PVOID);
bool hk_GetLockonState(PVOID p)
{
    if (bEnabled && bCallingSpellAim)
    {
        return bHasLockon;
    }
    //if (bCallingDirectionalAnim)
    //{
    //    bAnimDirectional = true;
    //    //if (Config.bUseInputOverride)
    //    //{
    //    //    return false;
    //    //}
    //}
    //return true;
    return tram_GetLockonState(p);
}

// called every frame, calls hk_GetLockonState when transitioning to a direction animation (roll, jump, quickstep, etc.)
void (*tram_DirectionalAnimation)(LPVOID, UINT32, LPVOID);
void hk_DirectionalAnimation(LPVOID rcx, UINT32 rdx, LPVOID r8)
{
    bCallingDirectionalAnim = true;
    tram_DirectionalAnimation(rcx, rdx, r8);
    bCallingDirectionalAnim = false;
}

// (?) picks a target point to have the projectiles track to
PVOID(*tram_SpellAim)(PVOID);
PVOID hk_SpellAim(PVOID p)
{
    bCallingSpellAim = true;
    PVOID pOut = tram_SpellAim(p);
    bCallingSpellAim = false;
    return pOut;
}

bool bCallingMovement;

void (*tram_FreeMovement)(LPVOID, float);
void hk_FreeMovement(LPVOID p, float f)
{
    bCallingMovement = true;
    bIsOnTorrent = *reinterpret_cast<LPBYTE>(UINT_PTR(p) + 0x1DC);
    tram_FreeMovement(p, f);
    bCallingMovement = false;
}

void (*tram_SetRotation)(LPVOID, LPVOID);
void hk_SetRotation(LPVOID p0, LPVOID p1)
{
    if (bHasLockon)
    {
        tram_SetRotation(p0, p1);
        return;
    }
    if (!bEnabled)
    {
        tram_SetRotation(p0, p1);
        return;
    }
    bool bSkipRotation = false;
    if (bCallingMovement && bIsIdle)
    {
        bSkipRotation = !(bIsOnTorrent);
    }
    if (!bSkipRotation)
    {
        tram_SetRotation(p0, p1);
    }
}


LPVOID pPlayerSkel;

/*
eldenring.exe+41945E - 48 8D A8 18FEFFFF     - lea rbp,[rax-000001E8]
eldenring.exe+419465 - 48 81 EC B0020000     - sub rsp,000002B0
eldenring.exe+41946C - 48 C7 45 C0 FEFFFFFF  - mov qword ptr [rbp-40],FFFFFFFFFFFFFFFE
eldenring.exe+419474 - 48 89 58 20           - mov [rax+20],rbx
eldenring.exe+419478 - 0F29 70 B8            - movaps [rax-48],xmm6
*/
std::vector<UINT16> PATTERN_FN_SKELETONTICK = { 0x48, 0x8D, 0xA8, 0x18, 0xFE, 0xFF, 0xFF, 0x48, 0x81, 0xEC, 0xB0, 0x02, 0x00, 0x00, 0x48, 0xC7, 0x45, 0xC0, 0xFE, 0xFF, 0xFF, 0xFF, 0x48, 0x89, 0x58, 0x20, 0x0F, 0x29, 0x70, 0xB8 };
constexpr int OFFSET_PATTERN_SKELETONTICK = -0xE;
// called for every skeletal mesh in the scene
void (*tram_SkeletonTick)(LPVOID, LPVOID, LPVOID);
void hk_SkeletonTick(LPVOID rcx, LPVOID rdx, LPVOID r8)
{
    tram_SkeletonTick(rcx, rdx, r8);
    if (rcx == pPlayerSkel)
    {
        // checks whether the player character has head tracking when talking to npcs, which
        // is added on top of the head-torso tracking from locking-on
        UINT_PTR addr = UINT_PTR(rcx);
        bHeadTracking = (*LPUINT(addr + 0x1788) == 2);
    }
}


/*
eldenring.exe+4E1B3E - E8 4D070000           - call eldenring.exe+4E2290
eldenring.exe+4E1B43 - 45 84 E4              - test r12b,r12b
eldenring.exe+4E1B46 - 74 55                 - je eldenring.exe+4E1B9D
eldenring.exe+4E1B48 - 49 8B 46 10           - mov rax,[r14+10]
eldenring.exe+4E1B4C - 48 8B 88 90010000     - mov rcx,[rax+00000190]
eldenring.exe+4E1B53 - 48 8B 89 E8000000     - mov rcx,[rcx+000000E8]
eldenring.exe+4E1B5A - E8 11FAF8FF           - call eldenring.exe+471570

E8 ???????? 45 84 E4 74 ?? 49 8B 46 10 48 8B 88 90010000 48 8B 89 E8000000 E8 ????????
*/
std::vector<UINT16> PATTERN_FN_GETSKELETON = { 0xE8, MASK, MASK, MASK, MASK, 0x45, 0x84, 0xE4, 0x74, MASK, 0x49, 0x8B, 0x46, 0x10, 0x48, 0x8B, 0x88, 0x90, 0x01, 0x00, 0x00, 0x48, 0x8B, 0x89, 0xE8, 0x00, 0x00, 0x00, 0xE8, MASK, MASK, MASK, MASK };

// finds the player character's skeletal mesh
// called once every few frames
void (*tram_GetSkeleton)(LPVOID, LPVOID, LPVOID);
void hk_GetSkeleton(LPVOID rcx, LPVOID rdx, LPVOID r8)
{
    UINT_PTR addr = UINT_PTR(r8);
    addr = *PUINT_PTR(addr + 0x190);
    addr = *PUINT_PTR(addr + 0x28);
    pPlayerSkel = reinterpret_cast<LPVOID>(addr);
    tram_GetSkeleton(rcx, rdx, r8);
}

/*
1.10
eldenring.exe+3F8D9E - 48 8B 8F 90010000     - mov rcx,[rdi+00000190]
eldenring.exe+3F8DA5 - 48 8D 45 A0           - lea rax,[rbp-60]
eldenring.exe+3F8DA9 - 4C 8D 4D C0           - lea r9,[rbp-40]
eldenring.exe+3F8DAD - 48 89 44 24 20        - mov [rsp+20],rax                 ; pattern starts
eldenring.exe+3F8DB2 - 4C 8D 45 B0           - lea r8,[rbp-50]
eldenring.exe+3F8DB6 - 0F28 F8               - movaps xmm7,xmm0
eldenring.exe+3F8DB9 - 48 8D 55 90           - lea rdx,[rbp-70]
eldenring.exe+3F8DBD - 48 8B 49 08           - mov rcx,[rcx+08]
eldenring.exe+3F8DC1 - E8 4AAA0000           - call eldenring.exe+403810        ; hook target call
eldenring.exe+3F8DC6 - F3 0F10 55 B0         - movss xmm2,[rbp-50]
eldenring.exe+3F8DCB - 0F28 C6               - movaps xmm0,xmm6
eldenring.exe+3F8DCE - F3 0F10 4D 90         - movss xmm1,[rbp-70]
eldenring.exe+3F8DD3 - E8 68ADD3FF           - call eldenring.exe+133B40

48 89 44 24 ?? 4C 8D ?? ?? 0F28 F8 48 8D ?? ?? 48 8B 49 ?? E8
*/

constexpr UINT_PTR OFFSET_PATTERN_AIM_DEADZONE_SPELL = (0xc1 - 0xad);
std::vector<UINT16> PATTERN_FN_AIM_DEADZONE_SPELL = { 0x48, 0x89, 0x44, 0x24, MASK, 0x4C, 0x8D, MASK, MASK, 0x0F, 0x28, 0xF8, 0x48, 0x8D, MASK, MASK, 0x48, 0x8B, 0x49, MASK, 0xE8 };
void (*tram_AimDeadzoneSpell)(LPVOID, LPVOID, LPVOID, LPVOID, LPVOID);
void hk_AimDeadzoneSpell(LPVOID rcx, LPVOID rdx, LPVOID r8, LPVOID r9, LPVOID s0)
{
    tram_AimDeadzoneSpell(rcx, rdx, r8, r9, s0);
    if (Config.bRemoveSpellAimDeadzone && bEnabled)
    {
        *reinterpret_cast<float*>(s0) = 0.f;
    }
}


constexpr char PATTERN_FN_COLLISION[] = "E8 ???????? 84 C0 0F84 ????0000 0F28 65 D0";

extern "C"
{
    LPVOID hkCaller_Collision = nullptr;
    void prehk_Collision();
}

/*
* params:
*	rcx: some static struct that points to other structs
*	rdx: (dword)
*	r8: (vec4) trace origin
*	r9: (vec3?) trace offset
*	[rsp+20]: trace sphere radius
*	[rsp+28]: (pointer) ???
*	[rsp+30]: 0
*	[rsp+38]: (pointer) out vectors?
*/
bool (*tram_Collision)(LPVOID, UINT, LPVOID, LPVOID, float, LPVOID, LPVOID, LPVOID);
extern "C" bool hk_Collision(LPVOID rcx, UINT rdx, glm::vec4 * r8, glm::vec3 * r9, float s0, LPVOID s1, LPVOID s2, LPVOID s3)
{
    SetCollisionTraceParams(tram_Collision, rcx, rdx, r8, r9, s0, s1, s2, s3);
    return tram_Collision(rcx, rdx, r8, r9, s0, s1, s2, s3);
}


static_assert(sizeof(glm::vec4) == 0x10);
struct GLockonTargetElement
{
    glm::vec4 vTargetPos;
    PAD(0x70);
    GLockonTargetElement* pNext;
};


/*
eldenring.exe+50D28C - BA B4000000           - mov edx,000000B4
eldenring.exe+50D291 - 48 8D 0D C8A74602     - lea rcx,[eldenring.exe+2977A60]
eldenring.exe+50D298 - E8 F3779601           - call eldenring.exe+1E74A90
eldenring.exe+50D29D - 48 8B 0D 3C3D7D03     - mov rcx,[eldenring.exe+3CE0FE0]
eldenring.exe+50D2A4 - 0F28 CE               - movaps xmm1,xmm6
eldenring.exe+50D2A7 - E8 84A91F00           - call eldenring.exe+707C30
eldenring.exe+50D2AC - 48 8B 8D E8E50100     - mov rcx,[rbp+0001E5E8]
eldenring.exe+50D2B3 - E8 78E27500           - call eldenring.exe+C6B530
eldenring.exe+50D2B8 - 48 8B 8D E8E50100     - mov rcx,[rbp+0001E5E8]
eldenring.exe+50D2BF - E8 2CCE7500           - call eldenring.exe+C6A0F0
eldenring.exe+50D2C4 - 0F28 74 24 20         - movaps xmm6,[rsp+20]
*/
constexpr const char* PATTERN_FN_UPDATE_LOCKON_TARGETS = "BA ???????? 48 8D 0D ???????? E8 ???????? 48 8B 0D ???????? 0F28 CE E8 ???????? 48 8B 8D ???????? E8";
constexpr const int OFFSET_FN_UPDATE_LOCKON_TARGETS = 27;
void (*tram_UpdateLockonTargets)(LPVOID, float);
void hk_UpdateLockonTargets(LPVOID rcx/*static struct*/, float xmm1)
{
    DeltaTime_UpdateLockonTargets = xmm1;
    tram_UpdateLockonTargets(rcx, xmm1);

    LockonTargetPositions.clear();
    GLockonTargetElement* pTarget = *reinterpret_cast<GLockonTargetElement**>(reinterpret_cast<uintptr_t>(rcx) + 0x10);
    while (pTarget)
    {
        glm::vec4 targetPos = pTarget->vTargetPos;
        LockonTargetPositions[pTarget] = targetPos;
        pTarget = pTarget->pNext;
    }

    //LOG_DEBUG << "Target count = " << LockonTargetPositions.size();
}


/*
eldenring.exe+3DB169 - E8 0259E6FF           - call eldenring.exe+240A70
eldenring.exe+3DB16E - 48 89 45 27           - mov [rbp+27],rax
eldenring.exe+3DB172 - 4C 8D 35 D7C65F02     - lea r14,[eldenring.exe+29D7850]
eldenring.exe+3DB179 - 4C 89 75 17           - mov [rbp+17],r14
eldenring.exe+3DB17D - 48 8B CB              - mov rcx,rbx
eldenring.exe+3DB180 - E8 BB9E0200           - call eldenring.exe+405040
eldenring.exe+3DB185 - F3 0F10 3D 538CE602   - movss xmm7,[eldenring.exe+3243DE0]
eldenring.exe+3DB18D - 84 C0                 - test al,al
eldenring.exe+3DB18F - 74 3E                 - je eldenring.exe+3DB1CF
eldenring.exe+3DB191 - 48 8D 4D 17           - lea rcx,[rbp+17]
eldenring.exe+3DB195 - E8 360CA100           - call eldenring.exe+DEBDD0
*/
constexpr const char* PATTERN_FN_ANIMATION_STATE_READ = "E8 ???????? F3 0F10 3D ???????? 84 C0 74 3E";
constexpr const int OFFSET_FN_ANIMATION_STATE_READ = 0;
bool (*tram_AnimationStateRead)(void*);
bool hk_AnimationStateRead(void* rcx)
{
    void* p = *reinterpret_cast<void**>(reinterpret_cast<uintptr_t>(rcx) + 8);
    p = *reinterpret_cast<void**>(reinterpret_cast<uintptr_t>(p) + 0x190);
    p = *reinterpret_cast<void**>(reinterpret_cast<uintptr_t>(p) + 0x8);
    p = reinterpret_cast<GAnimationState*>(p);
    std::memcpy(&AnimationState, p, sizeof(AnimationState));

    return tram_AnimationStateRead(rcx);
}

extern std::vector<std::unique_ptr<UMinHook>> Hooks;

#define PP(p) ((void**)&p)

bool InitHooks()
{

    Hooks.push_back(std::make_unique<UMinHook>("CameraTick", PATTERN_CAMERA_TICK, 0xFC - 0xD8, &hk_CameraTick, (LPVOID*)&tram_CameraTick));
    Hooks.push_back(std::make_unique<UMinHook>("PlayerAim", PATTERN_PLAYER_AIM, &hk_PLayerAim, (LPVOID*)&tram_PlayerAim));
    Hooks.push_back(std::make_unique<UMinHook>("LockonState", PATTERN_LOCKON_STATE, &hk_GetLockonState, (LPVOID*)&tram_GetLockonState));
    Hooks.push_back(std::make_unique<UMinHook>("SpellAim", PATTERN_SPELL_AIM, 9, &hk_SpellAim, (LPVOID*)&tram_SpellAim));
    Hooks.push_back(std::make_unique<UMinHook>("FreeMovement", PATTERN_FREE_MOVEMENT, 10, &hk_FreeMovement, (LPVOID*)&tram_FreeMovement));
    Hooks.push_back(std::make_unique<UMinHook>("SetRotation", PATTERN_SET_ROTATION, &hk_SetRotation, (LPVOID*)&tram_SetRotation));
    Hooks.push_back(std::make_unique<UMinHook>("GetSkeleton", PATTERN_FN_GETSKELETON, &hk_GetSkeleton, (LPVOID*)&tram_GetSkeleton));
    Hooks.push_back(std::make_unique<UMinHook>("SkeletonTick", PATTERN_FN_SKELETONTICK, OFFSET_PATTERN_SKELETONTICK, &hk_SkeletonTick, (LPVOID*)&tram_SkeletonTick));
    //CreateHook("DirectionalAnim", PATTERN_DIRECTIONAL_ANIMATION, &hk_DirectionalAnimation, (LPVOID*)&tram_DirectionalAnimation);
    Hooks.push_back(std::make_unique<UMinHook>("AimDeadzoneSpell", PATTERN_FN_AIM_DEADZONE_SPELL, OFFSET_PATTERN_AIM_DEADZONE_SPELL, &hk_AimDeadzoneSpell, (LPVOID*)&tram_AimDeadzoneSpell));
    Hooks.push_back(std::make_unique<UMinHook>("CameraCollision", PATTERN_FN_COLLISION, &prehk_Collision, (LPVOID*)&tram_Collision));

    Hooks.push_back(std::make_unique<UMinHook>("UpdateLockonTargets", PATTERN_FN_UPDATE_LOCKON_TARGETS, OFFSET_FN_UPDATE_LOCKON_TARGETS, hk_UpdateLockonTargets, PP(tram_UpdateLockonTargets)));
    Hooks.push_back(std::make_unique<UMinHook>("AnimationStateRead", PATTERN_FN_ANIMATION_STATE_READ, OFFSET_FN_ANIMATION_STATE_READ, hk_AnimationStateRead, PP(tram_AnimationStateRead)));
    
    for (auto& p : Hooks)
    {
        if (static_cast<int>(p->GetError()) > 0)
        {
            return false;
        }
    }
    
    return true;
}
