#pragma once

#include <glm-dx.h>
glm::vec4 CalcAutoTargetPos(glm::vec4 currentTarget, glm::vec4 playerPos, glm::vec3 cameraZ);
