
#include "pch.h"
#include "Globals.h"


HMODULE hCameraOffset = NULL;
void (*SetIgnoreCollisionHook)(bool) = nullptr;

GTraceParams TraceParams{ 0 };
UConfig Config;

std::unordered_map<void*, glm::vec4> LockonTargetPositions{};
float DeltaTime_UpdateLockonTargets{ 0.f };
GAnimationState AnimationState{ 0 };

glm::mat4 CameraTransform{ 0.f };
float CameraFoV = 48.f; // vertical fov in radians
glm::vec4 PivotPosUninterp{ 0.f };
float CameraMaxDistance{ 3.8f };
//glm::vec4 PrevPivotPos;
//glm::vec3 PivotVelocity;
bool bIsSprinting = false;
bool bIsOnTorrent = false;
bool bHeadTracking = false;

bool bCollisionHit = false;
float CollisionHitDistance = 3.8f;
glm::vec4 CollisionHitPos{ 0 };
glm::vec4 CollisionTraceEnd{ 0 };
glm::vec4 CollisionPosInterp{ 0 };

bool bEnabled{ true };

bool bHasLockon{ false };
bool bIsIdle = false;
bool bAnimDirectional = false;

#if USE_RESHADE
glm::vec2 HUDPos = { 0, 0 };
#endif