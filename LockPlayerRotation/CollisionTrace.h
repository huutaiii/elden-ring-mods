
#include <glm-dx.h>
#include <utility>

void SetCollisionTraceParams(LPVOID fp, LPVOID rcx, UINT rdx, glm::vec4* r8, glm::vec3* r9, float s0, LPVOID s1, LPVOID s2, LPVOID s3);
bool UseCollisionTrace();
bool CallCollisionTrace(glm::vec4 origin, glm::vec3 offset, float radius, std::pair<bool, UINT> physParam = std::pair(false, 0));
