#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>

#include <luajit/lua.hpp>
#include <glm-dx.h>
#define MODUTILS_MACROS
#define MODUTILS_PADDING
#define MODUTILS_GLOBAL_NAMESPACE 1
#define MATHUTILS_GLOBAL_NAMESPACE 1
#include <ModUtils.h>
#include <MathUtils.h>
#include <INIReader.h>
