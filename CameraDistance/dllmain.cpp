// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"

#include <format>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <cstring>

#define HOOKUTILS_GLOBAL_NAMESPACE 1
#define KEYBOARDUTILS_GLOBAL_NAMESPACE 1
#include <HookUtils.h>
#include <KeyboardUtils.h>

#pragma comment(lib, "minhook.x64.lib")
#pragma comment(lib, "lua51.lib")

std::string trim(const std::string &s)
{
	constexpr const char* const whitespaces = " \t\n";
	std::string out = s;
	while (out.size() > 0 && std::strchr(whitespaces, *out.begin()))
	{
		out.erase(out.begin());
	}
	while (out.size() > 0 && std::strchr(whitespaces, *(out.end() - 1)))
	{
		out.erase(out.end() - 1);
	}
	return out;
}

struct TCameraData
{
	PAD(0x10);
	glm::mat4 Transform;
	PAD(0x20);
	glm::mat4 PlayerTransform;
	// +B0
	glm::vec4 PosPivot;
	glm::vec4 PosPivotInterp;
	PAD(0x30);
	// +100
	glm::vec4 PosSocket;
	glm::vec4 PosSocketInterp;
	PAD(0x30);
	// +150
	// x: pitch, y: yaw
	glm::vec2 RotationEuler;
	glm::vec2 RotationEulerAlt;
	PAD(0x54);
	// +1B4
	float MaxDistance;
	PAD(0x128);
	union {
		// +2E0
		float LockonAimTolerance;
		PAD(0x10);
	};
	// +2F0
	glm::vec4 TargetPos;
	PAD(0x10);
	// +310
	union {
		bool bHasLockon;
		PAD(0x10);
	};
	PAD(0x140);
	// +460
	UINT32 ParamID;
	PAD(0x24);
	// +488
	union {
		bool bOnTorrent;
		PAD(8);
	};
};

std::mutex mtxLua;
lua_State* Lua;
std::atomic_bool DoLuaInit(true);
bool CloseLua = false;
ULog* Log;
std::vector<std::unique_ptr<UHook>> Hooks;

WNDPROC WndProcPrev = nullptr;
HMODULE g_hModule;

FDynamicTargetBlend<float> DistanceBlend;
FDynamicTargetBlend<float> LockonAlpha(0.f, 1.f);
FDynamicTargetBlend<float> TorrentAlpha(0.f, 1.f);
float LastDesiredDistance = 0.f;
float DistControlTarget = 0.f;
float DistControlInterp = 0.f;
float bUseDistance = true;
float DistToggleInterp = 1.f;

extern "C"
{
	TCameraData* pCameraData = nullptr;
	float DeltaTime = 0.f;
	bool bLockonStateChanged = false;
}

struct FConstants {
	float DurBlendDist = 0.8f;
};

struct FConfig : public FConstants {
	std::string DistLua = "d";
	std::string LockonDistLua = "d";
	float FoVMul = 1.0;
	glm::vec3 InterpSpeed = glm::vec3(1.f);
	glm::vec3 LockonInterpSpeed = glm::vec3(1.f);

	DWORD DelayHookWndProc = 0;

	float CtrlDelta = 1.f;
	float CtrlMin = -100.f;
	float CtrlMax = 100.f;
	float CtrlInterpSpeed = 10.f;

	struct FConfigKeys {
		UHotKey Toggle;
		UHotKey Inc;
		UHotKey Dec;
		UHotKey Reset;
		int MouseMod = 0;
		bool bInvert = false;
		bool bUseWheel = false;

		void Read(INIReader ini)
		{
			Toggle = UHotKey(ini.Get("control", "key_distance_toggle", ""));
			Inc = UHotKey(ini.Get("control", "key_distance_increase", ""));
			Dec = UHotKey(ini.Get("control", "key_distance_decrease", ""));
			Reset = UHotKey(ini.Get("control", "key_distance_reset", ""));
			MouseMod = GetModifier(trim(ini.Get("control", "wheel_modifier", "")));
			bUseWheel = ini.GetBoolean("control", "use_mouse_wheel", bUseWheel);
			bInvert = ini.GetBoolean("control", "invert_wheel", bInvert);
			//LOG.dprintln("%x %x %x %x", Toggle, Inc, Dec, MouseMod);
		}
	} Keys;

	void Read(INIReader ini)
	{
		DelayHookWndProc = ini.GetInteger("hooking", "delay_wndproc_hook", DelayHookWndProc);

		DistLua = ini.Get("camera_distance", "distance", DistLua);
		LockonDistLua = ini.Get("camera_distance", "distance_lockon", DistLua);

		FoVMul = ini.GetFloat("fov", "multiplier", FoVMul);

		InterpSpeed.xz = max(0.f, ini.GetFloat("pivot", "interp_speed", InterpSpeed.x));
		InterpSpeed.y = ini.GetFloat("pivot", "interp_speed_y", InterpSpeed.x);
		if (InterpSpeed.y <= 0.f)
		{
			InterpSpeed.y = InterpSpeed.x;
		}

		LockonInterpSpeed.xz = max(0.f, ini.GetFloat("pivot", "interp_speed_lockon", InterpSpeed.x));
		LockonInterpSpeed.y = ini.GetFloat("pivot", "interp_speed_y_lockon", InterpSpeed.y);
		if (LockonInterpSpeed.y <= 0.f)
		{
			LockonInterpSpeed.y = InterpSpeed.x;
		}

		CtrlDelta = ini.GetFloat("control", "control_delta", CtrlDelta);
		CtrlMin = ini.GetFloat("control", "control_min", CtrlMin);
		CtrlMax = ini.GetFloat("control", "control_max", CtrlMax);

		Keys.Read(ini);
	}

	void Read(std::string path)
	{
		INIReader ini(path);
		if (ini.ParseError())
		{
			Log->eprintln("Can't read config file: %d", ini.ParseError());
			return;
		}
		Read(ini);
	}
} Config;

extern "C" float CalcDistance(float inDist);

/*
eldenring.exe+3AFDD8 - 83 F8 07              - cmp eax,07
eldenring.exe+3AFDDB - 0F87 92040000         - ja eldenring.exe+3B0273
eldenring.exe+3AFDE1 - 41 8B 8C 85 CC023B00  - mov ecx,[r13+rax*4+003B02CC]
eldenring.exe+3AFDE9 - 49 03 CD              - add rcx,r13
eldenring.exe+3AFDEC - FF E1                 - jmp rcx
eldenring.exe+3AFDEE - 48 8B 4F 60           - mov rcx,[rdi+60]
eldenring.exe+3AFDF2 - 4D 8B CE              - mov r9,r14
eldenring.exe+3AFDF5 - 4C 8B C6              - mov r8,rsi
eldenring.exe+3AFDF8 - 41 0F28 C9            - movaps xmm1,xmm9
eldenring.exe+3AFDFC - E8 DF410000           - call eldenring.exe+3B3FE0

83 F8 07 0F87 ??????00 41 8B 8C 85 ???????? 49 03 CD FF E1 48 8B 4F ?? 4D 8B CE 4C 8B C6 41 0F28 C9 E8 ??????00
*/
constexpr char PATTERN_FN_CAMERATICK[] = "FF E1 48 8B 4F ?? 4D 8B CE 4C 8B C6 41 0F28 C9 E8 ??????00";
constexpr int OFFSET_FN_CAMERATICK = 0x10;

void (*tram_CameraTick)(void*, float, void*, void*);
void hk_CameraTick(void* rcx, float xmm1, void* r8, void* r9)
{
	static bool bLockonPrev = false;
	pCameraData = reinterpret_cast<TCameraData*>(rcx);
	DeltaTime = xmm1;
	bLockonStateChanged = bLockonPrev != pCameraData->bHasLockon;
	bLockonPrev = pCameraData->bHasLockon;

	DistControlInterp = InterpToF(DistControlInterp, DistControlTarget, Config.CtrlInterpSpeed, DeltaTime);
	DistToggleInterp = InterpToF(DistToggleInterp, float(bUseDistance), 2.f, DeltaTime);

	//if (bLockonStateChanged)
	//{
	//    DistanceBlend.Reset(LastDesiredDistance, Config.DurBlendDist);
	//}
	//LastDesiredDistance = DistanceBlend.Update(CalcDistance(*(float*)(uintptr_t(pCameraData) + 0x1B8)), DeltaTime);
	//pCameraData->MaxDistance = LastDesiredDistance;

	return tram_CameraTick(rcx, xmm1, r8, r9);
}

/*
eldenring.exe+3B5C7A - E8 51379500           - call eldenring.exe+D093D0
eldenring.exe+3B5C7F - 48 8D 4C 24 20        - lea rcx,[rsp+20]
eldenring.exe+3B5C84 - 0F28 F8               - movaps xmm7,xmm0
eldenring.exe+3B5C87 - E8 24399500           - call eldenring.exe+D095B0
eldenring.exe+3B5C8C - F3 44 0F10 3D C7D0E802  - movss xmm15,[eldenring.exe+3242D5C]

E8 ??????00 48 8D 4C 24 ?? 0F28 F8 E8 ??????00 F3 44 0F10 3D ????????
*/
constexpr char PATTERN_CAMERA_DISTANCE[] = "E8 ??????00 48 8D 4C 24 ?? 0F28 F8 E8 ??????00 F3 44 0F10 3D";

// lock mtxLua before calling
void UpdateLuaGlobals(lua_State* lua, TCameraData* pCamData)
{
	static bool LastHasLockon = false;
	static bool LastIsOnTorrent = false;
	if (LastHasLockon != pCamData->bHasLockon)
	{
		LastHasLockon = pCamData->bHasLockon;
		LockonAlpha.Reset();
		LockonAlpha.Easing = EaseInOutSine;
	}
	if (LastIsOnTorrent != pCamData->bOnTorrent)
	{
		LastIsOnTorrent = pCamData->bOnTorrent;
		TorrentAlpha.Reset();
		TorrentAlpha.Easing = EaseInOutSine;
	}
	LockonAlpha.Update((float)pCamData->bHasLockon, DeltaTime);
	TorrentAlpha.Update((float)pCamData->bOnTorrent, DeltaTime);

	lua_pushnumber(lua, lua_Number(pCamData->RotationEuler.x));
	lua_setglobal(lua, "pitch");
	lua_pushnumber(lua, lua_Number(LastDesiredDistance));
	lua_setglobal(lua, "prev_distance");
	lua_pushnumber(lua, lua_Number(DeltaTime));
	lua_setglobal(lua, "delta_time");
	lua_pushboolean(lua, int(pCamData->bHasLockon));
	lua_setglobal(lua, "has_lockon");
	lua_pushboolean(lua, int(pCamData->bOnTorrent));
	lua_setglobal(lua, "is_on_torrent");
	lua_pushnumber(lua, lua_Number(LockonAlpha.GetValue()));
	lua_setglobal(lua, "lockon_alpha");
	lua_pushnumber(lua, lua_Number(TorrentAlpha.GetValue()));
	lua_setglobal(lua, "torrent_alpha");
	lua_pushnumber(lua, lua_Number(DistControlInterp));
	lua_setglobal(lua, "control");
}

extern "C" float CalcDistance(float inDist)
{
	float out = inDist;
	if (mtxLua.try_lock())
	{
		if (Lua)
		{
			lua_pushnumber(Lua, lua_Number(inDist));
			lua_setglobal(Lua, "d");

			UpdateLuaGlobals(Lua, pCameraData);

			//Log->dprintln("p = %p, d = %f, pitch = %f", pCameraData, out, pCameraData->RotationEuler.x);
			//std::string script = std::format("out = ({})", pCameraData->bHasLockon ? Config.LockonDistLua : Config.DistLua);
			const std::string &expression = pCameraData->bHasLockon ? Config.LockonDistLua : Config.DistLua;
			std::string script{ "" };
			script.reserve(expression.size() + 6);
			script += "out=(";
			script += expression;
			script += ")";
			if (luaL_loadbuffer(Lua, script.c_str(), script.size(), "distance") || lua_pcall(Lua, 0, 0, 0))
			{
				ULog::Get().dprintln("lua error: %s", lua_tostring(Lua, -1));
			}
			else
			{
				lua_getglobal(Lua, "out");
				float result = float(lua_tonumber(Lua, -1));
				//Log->dprintln("out dist = %f", result);
				if (!std::isnan(result))
				{
					out = result;
				}
			}
			lua_settop(Lua, 0);
		}
		mtxLua.unlock();
	}
	return lerp(inDist, out, DistToggleInterp);
}

extern "C"
{
	void prehk_Distance();
	float hk_Distance(LPVOID);
}
float (*tram_Distance)(LPVOID);
float hk_Distance(LPVOID p)
{
	float out = tram_Distance(p);
	if (out <= 0.f)
	{
		return out;
	}
	return CalcDistance(out);
}

/*
eldenring.exe+3B5FF4 - E8 C7562B00           - call eldenring.exe+66B6C0
eldenring.exe+3B5FF9 - 44 0F28 D8            - movaps xmm11,xmm0
eldenring.exe+3B5FFD - E8 AE562B00           - call eldenring.exe+66B6B0
eldenring.exe+3B6002 - 44 0F28 E0            - movaps xmm12,xmm0
eldenring.exe+3B6006 - E8 D5562B00           - call eldenring.exe+66B6E0
eldenring.exe+3B600B - 44 0F28 C8            - movaps xmm9,xmm0
eldenring.exe+3B600F - E8 DC642B00           - call eldenring.exe+66C4F0
eldenring.exe+3B6014 - 45 0F2F E2            - comiss xmm12,xmm10
eldenring.exe+3B6018 - 0FB6 F8               - movzx edi,al

E8 ???????? 44 0F28 D8 E8 ???????? 44 0F28 E0 E8 ???????? 44 0F28 C8 ~~E8 ???????? 45 0F2F E2~~
*/
constexpr char PATTERN_CAMERA_DISTANCE_ANIM[] = "E8 ???????? 44 0F28 D8 E8 ???????? 44 0F28 E0 E8 ???????? 44 0F28 C8";

extern "C"
{
	void prehk_DistanceAnim();
	float hk_DistanceAnim(LPVOID);
}
float (*tram_DistanceAnim)(LPVOID);
float hk_DistanceAnim(LPVOID p)
{
	float out = tram_DistanceAnim(p);
	if (out <= 0.f)
	{
		return out;
	}
	return CalcDistance(out);
}


/*
eldenring.exe+3B609D - F3 0F10 83 B8010000   - movss xmm0,[rbx+000001B8]
eldenring.exe+3B60A5 - F3 0F5C F8            - subss xmm7,xmm0
eldenring.exe+3B60A9 - F3 0F59 FE            - mulss xmm7,xmm6
eldenring.exe+3B60AD - F3 0F58 F8            - addss xmm7,xmm0
eldenring.exe+3B60B1 - F3 0F11 BB B8010000   - movss [rbx+000001B8],xmm7
eldenring.exe+3B60B9 - 8B 83 B8010000        - mov eax,[rbx+000001B8]
F3 0F10 83 B8010000 F3 0F5C F8 F3 0F59 FE F3 0F58 F8 F3 0F11 BB B8010000 8B 83 B8010000
*/
constexpr char PATTERN_INLINE_CAMERA_DISTANCE[] = "F3 0F10 83 B8010000 F3 0F5C F8 F3 0F59 FE F3 0F58 F8 F3 0F11 BB B8010000 8B 83 B8010000";

extern "C"
{
	void asmhk_DistanceInline();
}

extern "C" float hk_DistanceInline(float in)
{
	if (bLockonStateChanged)
	{
		DistanceBlend.Reset(LastDesiredDistance, Config.DurBlendDist);
	}
	LastDesiredDistance = DistanceBlend.Update(CalcDistance(in), DeltaTime);
	return LastDesiredDistance;
}

/*
eldenring.exe+3B6A94 - 4C 8D 4E 30           - lea r9,[rsi+30]
eldenring.exe+3B6A98 - 4C 8D 83 8C040000     - lea r8,[rbx+0000048C]    ; *vec2 delta_time (?)
eldenring.exe+3B6A9F - 48 8D 8B 10020000     - lea rcx,[rbx+00000210]   ; *{ float[4] unknown; vec4 target_pos; vec4 current_pos(?) }
eldenring.exe+3B6AA6 - 48 8D 45 C0           - lea rax,[rbp-40]
eldenring.exe+3B6AAA - 48 89 44 24 28        - mov [rsp+28],rax         ; *vec3 interp_speed
eldenring.exe+3B6AAF - 48 89 7C 24 20        - mov [rsp+20],rdi
eldenring.exe+3B6AB4 - 48 8D 55 D0           - lea rdx,[rbp-30]
eldenring.exe+3B6AB8 - E8 13D8A100           - call eldenring.exe+DD42D0
eldenring.exe+3B6ABD - 0F28 00               - movaps xmm0,[rax]
eldenring.exe+3B6AC0 - 66 0F7F 07            - movdqa [rdi],xmm0

~~4C 8D 4E 30 4C 8D 83 8C040000 48 8D 8B 10020000~~ 48 8D 45 C0 48 89 44 24 28 48 89 7C 24 20 48 8D 55 D0 E8 ????????*/
constexpr char PATTERN_PIVOT_INTERP[] = "48 8D 45 C0 48 89 44 24 28 48 89 7C 24 20 48 8D 55 D0 E8";
constexpr int OFFSET_PIVOT_INTERP = 0xb8 - 0xa6;

struct TPivotInterpIn
{
	float unknown[4];
	glm::vec4 target_pos;
	glm::vec4 current_pos;
};

glm::vec4* (*tram_PivotInterp)(LPVOID, LPVOID, LPVOID, LPVOID, LPVOID, LPVOID);
glm::vec4* hk_PivotInterp(TPivotInterpIn* rcx, LPVOID rdx, glm::vec2* r8, LPVOID r9, void* s0, glm::vec3* s1)
{
	*s1 *= pCameraData->bHasLockon ? Config.LockonInterpSpeed : Config.InterpSpeed;
	return tram_PivotInterp(rcx, rdx, r8, r9, s0, s1);
}

/*
eldenring.exe+3B5CC4 - 48 8D 4C 24 20        - lea rcx,[rsp+20]
eldenring.exe+3B5CC9 - 44 0F28 C8            - movaps xmm9,xmm0
eldenring.exe+3B5CCD - E8 8E399500           - call eldenring.exe+D09660
eldenring.exe+3B5CD2 - 80 BB 88040000 00     - cmp byte ptr [rbx+00000488],00
eldenring.exe+3B5CD9 - 44 0F28 E0            - movaps xmm12,xmm0        ; vertical fov (deg)

48 8D 4C 24 20 44 0F28 C8 E8 ???????? 80 BB 88040000 00 44 0F28 E0
*/
constexpr char PATTERN_FOV[] = "48 8D 4C 24 20 44 0F28 C8 E8 ???????? 80 BB 88040000 00 44 0F28 E0";
constexpr int OFFSET_FOV = 0xcd - 0xc4;

float (*tram_FoV)(LPVOID);
float hk_FoV(LPVOID rcx)
{
	return tram_FoV(rcx) * Config.FoVMul;
}


LRESULT WINAPI HookWndProc(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	//if (msg == WM_XBUTTONDOWN) __debugbreak();
	if (Config.Keys.Toggle.IsPressed(msg, wparam, lparam))
	{
		bUseDistance = !bUseDistance;
	}
	if (Config.Keys.Inc.IsPressed(msg, wparam, lparam))
	{
		DistControlTarget += Config.CtrlDelta;
	}
	if (Config.Keys.Dec.IsPressed(msg, wparam, lparam))
	{
		DistControlTarget -= Config.CtrlDelta;
	}
	if (Config.Keys.Reset.IsPressed(msg, wparam, lparam))
	{
		DistControlTarget = 0;
	}

	switch (msg)
	{
	case WM_MOUSEWHEEL:
		if (Config.Keys.bUseWheel && AreModifiersHeld({ Config.Keys.MouseMod }))
		{
			short wheel = static_cast<short>(HIWORD(wparam)) / WHEEL_DELTA;
			LOG.dprintln("wheel delta: %d", wheel);
			DistControlTarget += (Config.Keys.bInvert ? -1.f : 1.f) * wheel;
		}
		break;
	}
	DistControlTarget = clamp(DistControlTarget, Config.CtrlMin, Config.CtrlMax);

	return CallWindowProcW(WndProcPrev, hWnd, msg, wparam, lparam);
}

constexpr bool USE_INLINE_HOOK = true;
constexpr bool HOOK_WNDPROC = true;

std::string ReadFile(const std::filesystem::path& path)
{
	std::ifstream sf(path);
	std::stringstream ss;
	ss << sf.rdbuf();
	return ss.str();
}

int SearchAndExecuteLua()
{
	int errCount = 0;
	if (Lua == nullptr)
	{
		LOG.eprintln("Invalid Lua state");
		return -1;
	}
	std::string modDir = std::format("{}\\{}", GetDLLDirectory(g_hModule), TARGET_NAME);
	LOG.dprintln("iterating directory: %s", modDir.c_str());
	if (std::filesystem::exists(modDir))
	{
		for (const auto& entry : std::filesystem::directory_iterator(modDir))
		{
			if (entry.is_regular_file() && entry.path().extension().string() == ".lua")
			{
				LOG.dprintln("found lua script: %s", entry.path().string().c_str());
				std::string script = ReadFile(entry.path());
				LOG.dprintln("executing script: \n%s", script.c_str());
				if (luaL_loadbuffer(Lua, script.c_str(), script.size(), entry.path().filename().string().c_str()) || lua_pcall(Lua, 0, 0, 0))
				{
					++errCount;
					LOG.println("Failed to execute '%s', error: %s", entry.path().string().c_str(), lua_tostring(Lua, -1));
				}
			}
			lua_settop(Lua, 0);
		}
	}
	if (errCount > 0)
	{
		ULog::Get().dprintln("%d Lua scripts failed to execute successfully", errCount);
	}
	return errCount;
}

DWORD WINAPI ThreadMain(LPVOID lpParam)
{
	Hooks.push_back(
		std::make_unique<UMinHook>(
			"CameraTick",
			PATTERN_FN_CAMERATICK,
			OFFSET_FN_CAMERATICK,
			&hk_CameraTick,
			(LPVOID*)(&tram_CameraTick)
		)
	);
	if constexpr (USE_INLINE_HOOK)
	{
		std::vector<uint16_t> patternDistHookInline = StringtoScanPattern(PATTERN_INLINE_CAMERA_DISTANCE);
		Hooks.push_back(
			std::make_unique<UHookInline>(
				"MaxDistanceInline",
				patternDistHookInline,
				patternDistHookInline.size(),
				&asmhk_DistanceInline
			)
		);
		Hooks[Hooks.size() - 1]->Enable();
	}
	else
	{
		Hooks.push_back(
			std::make_unique<UMinHook>(
				"MaxDistance",
				PATTERN_CAMERA_DISTANCE,
				&prehk_Distance,
				(LPVOID*)(&tram_Distance)
			)
		);
		Hooks.push_back(
			std::make_unique<UMinHook>(
				"MaxDistanceAnim",
				PATTERN_CAMERA_DISTANCE_ANIM,
				&prehk_DistanceAnim,
				(LPVOID*)(&tram_DistanceAnim)
			)
		);
	}
	Hooks.push_back(
		std::make_unique<UMinHook>(
			"FoV",
			PATTERN_FOV,
			OFFSET_FOV,
			&hk_FoV,
			(LPVOID*)(&tram_FoV)
		)
	);
	Hooks.push_back(
		std::make_unique<UMinHook>(
			"PivotInterp",
			PATTERN_PIVOT_INTERP,
			OFFSET_PIVOT_INTERP,
			&hk_PivotInterp,
			(LPVOID*)(&tram_PivotInterp)
		)
	);

	return 0;
}

DWORD ThreadHookWndProc(LPVOID lpParam)
{
	if constexpr (HOOK_WNDPROC)
	{
		LOG_DEBUG << std::format("wndproc hook delay: {}", Config.DelayHookWndProc);
		Sleep(Config.DelayHookWndProc);
		LOG_INFO << "Attempting to hook WNDPROC";
		HWND hwnd = FindWindowHandle(L"ELDEN RING", true);
		if (hwnd)
		{
			//ULog::Get().println("Found window: %s %p", GetWinAPIString(GetWindowTextA, hwnd).c_str(), hwnd);
			SetLastError(0);
			WndProcPrev = (WNDPROC)SetWindowLongPtrA(hwnd, GWLP_WNDPROC, LONG_PTR(&HookWndProc));
			LOG.println("hook WNDPROC: %d %p %p", GetLastError(), WndProcPrev, &HookWndProc);
		}
	}
	return 0;
}

#define LS_IMPL(pf, s) pf##s
#define LS(s) LS_IMPL(L, s)

DWORD WINAPI ThreadConfigWatch(LPVOID lpParam)
{
	static constexpr size_t FILE_NOTIFY_BUFFER_SIZE = 4096;

	std::string filename = "config.ini";
	std::wstring wfilename = L"config.ini";

	std::string modDir = GetDLLDirectory(g_hModule) + "\\" TARGET_NAME;
	//Config.Read(modDir + "\\" + filename);

	//int errorCount = SearchAndExecuteLua();
	while (1)
	{
		char notifybuf[FILE_NOTIFY_BUFFER_SIZE];
		DWORD numBytes;
		HANDLE hDir = CreateFileA(modDir.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, 0);
		if (ReadDirectoryChangesW(hDir, &notifybuf, FILE_NOTIFY_BUFFER_SIZE, FALSE, FILE_NOTIFY_CHANGE_LAST_WRITE, &numBytes, 0, 0))
		{
			FILE_NOTIFY_INFORMATION* pNotify = reinterpret_cast<FILE_NOTIFY_INFORMATION*>(&notifybuf);
			if (numBytes)
			{
				while (1)
				{
					std::wstring wsName(pNotify->FileName, pNotify->FileNameLength / sizeof(WCHAR));
					if (wsName == wfilename)
					{
						std::string filename;
						std::transform(wsName.begin(), wsName.end(), std::back_inserter(filename), [](wchar_t c) {
							return (char)c;
							});
						ULog::Get().println("Config file changed");
						Sleep(300); // workaround for some editors like vscode
						Config.Read(modDir + "\\" + filename);
					}
					else
						if (wsName.ends_with(L".lua"))
						{
							LOG.println("Lua scripts changed");
							Sleep(500);

							//std::lock_guard lock(mtxLua);
							DoLuaInit.store(true);
							//if (Lua)
							//{
							//    std::lock_guard lock(mtxLua);
							//    lua_settop(Lua, 0);
							//    lua_close(Lua);
							//    //Lua = nullptr;
							//    LOG.dprintln("lua closed");
							//    /*Lua = lua_open();
							//    luaL_openlibs(Lua);
							//    SearchAndExecuteLua();*/
							//}
						}
					if (pNotify->NextEntryOffset == 0) break;
					pNotify += pNotify->NextEntryOffset;
				}
			}
			else
			{
				ULog::Get().println("Couldn't setup config file watch");
				break;
			}
		}
	}

	return 0;
}

//static void* LuaAlloc(void* ud, void* ptr, size_t osize, size_t nsize)
//{
//    if (nsize == 0)
//    {
//        free(ptr);
//        return nullptr;
//    }
//    return realloc(ptr, nsize);
//}

DWORD WINAPI ThreadLua(LPVOID lpParam)
{
	while (!CloseLua)
	{
		if (DoLuaInit.load())
		{
			std::lock_guard lock(mtxLua);
			DoLuaInit.store(false);
			if (Lua)
			{
				LOG.dprintln("closing Lua state");
				lua_close(Lua);
				Lua = nullptr;
			}
			//Sleep(5000);
			LOG.dprintln("initializing Lua state");
			//lua_settop(lua, 0);
			Lua = lua_open();
			luaL_openlibs(Lua);
			SearchAndExecuteLua();
		}
	}
	lua_close(Lua);
	return 0;
}

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	{
		g_hModule = hModule;
		DisableThreadLibraryCalls(hModule);
		std::string dir = GetDLLDirectory(hModule);

		HWND mainWindow = NULL/*FindWindowHandle(L"ELDEN RING")*/;

		SetLastError(0);
		HMODULE hmodLua = LoadLibraryA((dir + "\\bin\\lua51.dll").c_str());
		if (!hmodLua)
		{
			MessageBoxA(mainWindow, std::format("Cannot load lua5.1: {}", GetLastError()).c_str(), TARGET_NAME, MB_ICONERROR);
			return FALSE;
		}
		//Lua = lua_open();
		//luaL_openlibs(Lua);

		ULog::FileName = dir + "\\" TARGET_NAME "\\log.txt";
		ULog::ModuleName = TARGET_NAME;
		Log = &ULog::Get();

		SetLastError(0);
		HMODULE minhook = LoadLibraryA((dir + "\\bin\\minhook.x64.dll").c_str());
		if (!minhook)
		{
			MessageBoxA(mainWindow, std::format("Cannot load MinHook: {}", GetLastError()).c_str(), TARGET_NAME, MB_ICONERROR);
			return FALSE;
		}
		MH_Initialize();

		std::string modDir = GetDLLDirectory(g_hModule) + "\\" TARGET_NAME;
		Config.Read(modDir + "\\config.ini");

		CreateThread(0, 0, &ThreadMain, 0, 0, 0);
		CreateThread(0, 0, &ThreadHookWndProc, 0, 0, 0);
		CreateThread(0, 0, &ThreadConfigWatch, 0, 0, 0);
		CreateThread(0, 0, &ThreadLua, 0, 0, 0);

		Log->println("config struct: %p", &Config);
		break;
	}
	case DLL_PROCESS_DETACH:
	{
		LOG.dprintln("closing " TARGET_NAME);
		MH_Uninitialize();
		//if (Lua)
		//{
		//    lua_close(Lua);
		//    Lua = nullptr;
		//}
		break;
	}
	}
	return TRUE;
}

