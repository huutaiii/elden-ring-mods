
extern hk_Distance : proto
extern hk_DistanceAnim : proto
extern pCameraData : qword

extern hk_DistanceInline : proto

.code
	prehk_Distance proc
		;mov [pCameraData], rsi
		jmp [hk_Distance]
	prehk_Distance endp

	prehk_DistanceAnim proc
		;mov [pCameraData], rsi
		jmp [hk_DistanceAnim]
	prehk_DistanceAnim endp


	asmhk_DistanceInline proc
		movd xmm0, eax

		lea rsp, [rsp - 28h]
		call hk_DistanceInline
		lea rsp, [rsp + 28h]

		movd eax, xmm0
		ret
	asmhk_DistanceInline endp
end