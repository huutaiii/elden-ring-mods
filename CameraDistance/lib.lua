
-- '*.lua' files in the config directory will get executed once the mod is loaded and when the Lua scripts are modified
-- *** be careful copying lua scripts into the same directory ***
-- reference: https://www.lua.org/manual/5.1/
------------------------------------------------------------------------------------------------------------------------


function clamp(x, min, max)
    return math.max(min, math.min(max, x))
end


function saturate(x)
    return math.max(0, math.min(1, x))
end


function lerp(x, y, a)
    a = saturate(a)
    return y * a + x * (1 - a)
end


-- interpolates a scalar value, gets slower as it approaches the target
function interpolate(current, target, speed, deltatime)
    return lerp(current, target, saturate(speed * deltatime))
end


local _bool_interp = {}
function NewBoolInterp(bVal)
    local alpha = bVal and 1 or 0
    return function (bTarget, speed, deltatime)
        alpha = interpolate(alpha, bTarget and 1 or 0, speed, deltatime)
        return alpha
    end
end


-- interplates a boolean value between 0..1
function interp_bool(id, target, speed, deltatime)
    if (_bool_interp[id] == nil) then
        _bool_interp[id] = NewBoolInterp(target)
    end
    return _bool_interp[id](target, speed, deltatime)
end

