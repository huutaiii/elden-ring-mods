
#include "pch.h"

#include <filesystem>
#include <memory>
#include <unordered_map>

namespace std { namespace fs = filesystem; }


/*
eldenring.exe+3B6492 - 48 8B CE              - mov rcx,rsi
eldenring.exe+3B6495 - E8 E6220000           - call eldenring.exe+3B8780
eldenring.exe+3B649A - 4D 8B C6              - mov r8,r14
eldenring.exe+3B649D - 41 0F28 CF            - movaps xmm1,xmm15
eldenring.exe+3B64A1 - 48 8B CE              - mov rcx,rsi
eldenring.exe+3B64A4 - E8 970A0000           - call eldenring.exe+3B6F40
eldenring.exe+3B64A9 - 0FB6 86 10030000      - movzx eax,byte ptr [rsi+00000310]
eldenring.exe+3B64B0 - 4C 8D 45 00           - lea r8,[rbp+00]
eldenring.exe+3B64B4 - 48 8D 54 24 40        - lea rdx,[rsp+40]
eldenring.exe+3B64B9 - 88 86 9B040000        - mov [rsi+0000049B],al
eldenring.exe+3B64BF - 48 8B CE              - mov rcx,rsi
*/

constexpr const char* PATTERN_FN_WOBBLE = R"(E8 ???????? 0FB6 86 10030000 4C 8D 45 00 48 8D 54 24 40 88 86 9B040000)";

struct GCameraData
{
    PAD(0x140);
    float InvWobbleAlpha;
};

void (*Tram_Wobble)(LPVOID);
void Hook_Wobble(GCameraData* p)
{
    p->InvWobbleAlpha = 1.f;
    Tram_Wobble(p);
}


std::unordered_map<std::string, std::unique_ptr<HookUtils::UMinHook>> Hooks;
DWORD WINAPI Main(LPVOID lpParams)
{
    MH_Initialize();

    Hooks["Wobble"] = std::make_unique<HookUtils::UMinHook>("Wobble", PATTERN_FN_WOBBLE, Hook_Wobble, (void**)&Tram_Wobble);

    return 0;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    if (ul_reason_for_call == DLL_PROCESS_ATTACH)
    {
        DisableThreadLibraryCalls(hModule);

        std::fs::path modDir = std::fs::path(ModUtils::GetWinAPIString(GetModuleFileNameW, hModule)).parent_path() / TARGET_NAME;
        std::fs::create_directories(modDir);
        ModUtils::ULog::FileName = modDir / TARGET_NAME ".log";
        ModUtils::ULog::ModuleName = TARGET_NAME;

        CreateThread(0, 0, &Main, 0, 0, 0);
    }
    if (ul_reason_for_call == DLL_PROCESS_DETACH)
    {
        MH_Uninitialize();
    }
    return TRUE;
}

