#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>

#define MODUTILS_MACROS 1
#define MODUTILS_PADDING 1
#include <ModUtils.h>
#include <HookUtils.h>

#include <MinHook.h>